package gamePAOO.Graphics;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public class Assets {
	
	private static final int width = 16, height = 16;
	private static final int playerwidth=50, playerheight=37;
	private static final int banditwidth=48, banditheight=48;
	private static final int skeletonwidth=24, skeletonheight=32;
	public static BufferedImage  dirt,dirtStone, wall, platform,background,menuBackground, box, door, key,bandit_Dead,heart, lava, gameOver, keyboard, won,wonGame;
	public static BufferedImage[] player_IDLE_Right,player_IDLE_Left, playerRun_Right,playerRun_Left, fall_Right,fall_Left,
			jump_Right,jump_Left,attack_Right,attack_Left,player_die,
			skeleton_Idle_Left,skeleton_Idle_Right,skeleton_Attack_Left,skeleton_Attack_Right,skeleton_Hit_Left, skeleton_Hit_Right,skeleton_Dead, skeleton_Run_Right, skeleton_Run_Left,
			bandit_Idle_Left,bandit_Idle_Right,bandit_Attack_Left,bandit_Attack_Right,bandit_Hit_Left, bandit_Hit_Right, bandit_Run_Right, bandit_Run_Left;

	public static BufferedImage[] btn_start,btn_exit,btn_options, btn_back, btn_new, btn_load,btn_save, btn_continue,level1, level2;

	public static BufferedImage flip(BufferedImage img)
	{
		AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		tx.translate(-img.getWidth(null),0);
		AffineTransformOp op = new AffineTransformOp(tx,
				AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		img = op.filter(img, null);
		return img;
	}


	public static void init(){
		SpriteSheet sheet2 = new SpriteSheet(ImageLoader.loadImage("/textures/walls.png"));
		SpriteSheet playersheet = new SpriteSheet(ImageLoader.loadImage("/textures/adventurer-Sheet.png"));
		SpriteSheet banditsheet = new SpriteSheet(ImageLoader.loadImage("/textures/HeavyBandit.png"));
		SpriteSheet sk_Attack = new SpriteSheet(ImageLoader.loadImage("/textures/Skeleton Attack.png"));
		SpriteSheet sk_Dead = new SpriteSheet(ImageLoader.loadImage("/textures/Skeleton Dead.png"));
		SpriteSheet sk_Hit = new SpriteSheet(ImageLoader.loadImage("/textures/Skeleton Hit.png"));
		SpriteSheet sk_Idle = new SpriteSheet(ImageLoader.loadImage("/textures/Skeleton Idle.png"));
		SpriteSheet sk_Run = new SpriteSheet(ImageLoader.loadImage("/textures/Skeleton Walk.png"));

		
		SpriteSheet groundsheet = new SpriteSheet(ImageLoader.loadImage("/textures/ground.png"));
		SpriteSheet environment=new SpriteSheet(ImageLoader.loadImage("/textures/environment.png"));
		SpriteSheet hearts=new SpriteSheet(ImageLoader.loadImage("/textures/heart_animated_1.png"));
		SpriteSheet environment_objects=new SpriteSheet(ImageLoader.loadImage("/textures/env_objects.png"));
		


		player_IDLE_Right=new BufferedImage[4];
		player_IDLE_Left=new BufferedImage[4];
		playerRun_Right=new BufferedImage[6];
		playerRun_Left=new BufferedImage[6];
		fall_Right=new BufferedImage[2];
		fall_Left=new BufferedImage[2];
		jump_Right=new BufferedImage[4];
		jump_Left=new BufferedImage[4];
		attack_Right=new BufferedImage[5];
		attack_Left=new BufferedImage[5];
		player_die=new BufferedImage[6];


		bandit_Idle_Left=new BufferedImage[4];
		bandit_Idle_Right=new BufferedImage[4];
		bandit_Attack_Left=new BufferedImage[8];
		bandit_Attack_Right=new BufferedImage[8];
		bandit_Run_Left=new BufferedImage[8];
		bandit_Run_Right=new BufferedImage[8];
		bandit_Hit_Left=new BufferedImage[4];
		bandit_Hit_Right=new BufferedImage[4];


		skeleton_Attack_Left=new BufferedImage[18];
		skeleton_Attack_Right=new BufferedImage[18];
		skeleton_Hit_Left=new BufferedImage[8];
		skeleton_Hit_Right=new BufferedImage[8];
		skeleton_Idle_Left=new BufferedImage[11];
		skeleton_Idle_Right=new BufferedImage[11];
		skeleton_Run_Left=new BufferedImage[13];
		skeleton_Run_Right=new BufferedImage[13];
		skeleton_Dead=new BufferedImage[13];


		btn_start=new BufferedImage[2];
		btn_exit=new BufferedImage[2];
		btn_options=new BufferedImage[2];
		btn_back=new BufferedImage[2];
		btn_new=new BufferedImage[2];
		btn_load=new BufferedImage[2];
		btn_save=new BufferedImage[2];
		btn_continue=new BufferedImage[2];
		level1=new BufferedImage[2];
		level2=new BufferedImage[2];

		btn_start[0]=ImageLoader.loadImage("/menu/buttons/play.png");
		btn_exit[0]=ImageLoader.loadImage("/menu/buttons/exit.png");
		btn_options[0]=ImageLoader.loadImage("/menu/buttons/options.png");
		btn_back[0]=ImageLoader.loadImage("/menu/buttons/back.png");
		btn_new[0]=ImageLoader.loadImage("/menu/buttons/New_Game.png");
		btn_load[0]=ImageLoader.loadImage("/menu/buttons/Load_Game.png");
		btn_save[0]=ImageLoader.loadImage("/menu/buttons/Save_Game.png");
		btn_continue[0]=ImageLoader.loadImage("/menu/buttons/Continue.png");
		level1[0]=ImageLoader.loadImage("/menu/buttons/Nivelul1.png");
		level2[0]=ImageLoader.loadImage("/menu/buttons/Nivelul2.png");
		btn_start[1]=ImageLoader.loadImage("/menu/buttons/play_hovered.png");
		btn_exit[1]=ImageLoader.loadImage("/menu/buttons/exit_hovered.png");
		btn_options[1]=ImageLoader.loadImage("/menu/buttons/options_hovered.png");
		btn_back[1]=ImageLoader.loadImage("/menu/buttons/back_hovered.png");
		btn_new[1]=ImageLoader.loadImage("/menu/buttons/New_Game_hovered.png");
		btn_load[1]=ImageLoader.loadImage("/menu/buttons/Load_Game_hovered.png");
		btn_save[1]=ImageLoader.loadImage("/menu/buttons/Save_Game_Hovered.png");
		btn_continue[1]=ImageLoader.loadImage("/menu/buttons/Continue_hovered.png");
		level1[1]=ImageLoader.loadImage("/menu/buttons/Nivelul1_hovered.png");
		level2[1]=ImageLoader.loadImage("/menu/buttons/Nivelul2_hovered.png");




		for(int i=0;i<4;i++)
		{
			player_IDLE_Right[i]=playersheet.crop(i*playerwidth,0,playerwidth,playerheight);
			player_IDLE_Left[i]=flip(player_IDLE_Right[i]);
			jump_Right[i]=playersheet.crop(i*playerwidth,2*playerheight,playerwidth,playerheight);
			jump_Left[i]=flip(jump_Right[i]);
			bandit_Idle_Right[i]=banditsheet.crop(i*banditwidth,0,banditwidth,banditheight);
			bandit_Idle_Left[i]=flip(bandit_Idle_Right[i]);
			bandit_Hit_Right[i]=banditsheet.crop(i*banditwidth,4*banditheight,banditwidth,banditheight);
			bandit_Hit_Left[i]=flip(bandit_Hit_Right[i]);


		}

		for(int i=0;i<5;i++) {
			attack_Right[i] = playersheet.crop(i*playerwidth,6*playerheight,playerwidth,playerheight);
			attack_Left[i]=flip(attack_Right[i]);
		}

		for(int i=0;i<6;i++)
		{
			playerRun_Right[i]=playersheet.crop((i+1)*playerwidth,playerheight,playerwidth,playerheight);
			playerRun_Left[i]=flip(playerRun_Right[i]);
			player_die[i]=playersheet.crop(i*playerwidth,10*playerheight,playerwidth,playerheight);
		}


		for(int i=0;i<8;i++){
			skeleton_Hit_Right[i]=sk_Hit.crop(i*30,0,30,skeletonheight);
			skeleton_Hit_Left[i]=flip(skeleton_Hit_Right[i]);
			bandit_Attack_Right[i]=banditsheet.crop(i*banditwidth,2*banditwidth,banditwidth,banditheight);
			bandit_Attack_Left[i]=flip(bandit_Attack_Right[i]);
			bandit_Run_Right[i]=banditsheet.crop(i*banditwidth,banditwidth,banditwidth,banditheight);
			bandit_Run_Left[i]=flip(bandit_Run_Right[i]);

		}
		for(int i=0;i<11;i++){
			skeleton_Idle_Right[i]=sk_Idle.crop(i*skeletonwidth,0,skeletonwidth,skeletonheight);
			skeleton_Idle_Left[i]=flip(skeleton_Idle_Right[i]);

		}

		for(int i=0;i<13;i++)
		{
			skeleton_Run_Right[i]=sk_Run.crop(i*22,0,22,33);
			skeleton_Run_Left[i]=flip(skeleton_Run_Right[i]);
			skeleton_Dead[i]=sk_Dead.crop(i*33,0,33,skeletonheight);

		}

		for(int i=0;i<18;i++){
			skeleton_Attack_Right[i]=sk_Attack.crop(i*43,0,43,37);
			skeleton_Attack_Left[i]=flip(skeleton_Attack_Right[i]);
		}


		menuBackground=ImageLoader.loadImage("/menu/menuBackground.jpg");
		keyboard=ImageLoader.loadImage("/menu/keyboard.png");
		won=ImageLoader.loadImage("/menu/buttons/Congrats.png");
		wonGame=ImageLoader.loadImage("/menu/buttons/WonGame.png");
		gameOver=ImageLoader.loadImage("/menu/buttons/GameOver.png");
		key=ImageLoader.loadImage("/textures/key.png");
		heart=hearts.crop(0,0,20,17);
		bandit_Dead=banditsheet.crop(4*banditwidth,4*banditheight,banditwidth,banditheight);


		wall = sheet2.crop(0, 0, width, height);
		platform=groundsheet.crop(16*width, height,width,height);
		background=groundsheet.crop(17*width, height,width,height);
		dirt=groundsheet.crop(6*width, 4*height,width,height);
		dirtStone=groundsheet.crop(width,height,width,height);
		lava=groundsheet.crop(0,5*height,4*width,height);
		box=environment.crop(0,20*height,2*width,2*height);
		door=environment_objects.crop(17*width,0,3*width,4*height);



	}
	
}
