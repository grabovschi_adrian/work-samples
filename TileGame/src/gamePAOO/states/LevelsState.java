package gamePAOO.states;

import gamePAOO.Graphics.Assets;
import gamePAOO.Handler;
import gamePAOO.entities.Static.Lava;
import gamePAOO.entities.creatures.Bandit;
import gamePAOO.entities.creatures.Skeleton;
import gamePAOO.tiles.Tile;
import gamePAOO.ui.ClickListener;
import gamePAOO.ui.UIImageButton;
import gamePAOO.ui.UIManager;
import gamePAOO.worlds.World;

import java.awt.*;

public class LevelsState extends State {

	private UIManager uiManager;
	private static UIManager copy_uiManager;



	public LevelsState(Handler handler){

		super(handler);
		uiManager=new UIManager(handler);
		handler.getMouseManager().setUiManager(uiManager);
		copy_uiManager=uiManager;


		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32, handler.getHeight()/2-64,64,32, Assets.btn_back, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(MenuState.getCopy_uiManager());
				State.setState(handler.getGame().getMenuState());

			}
		}));
		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32,handler.getHeight()/2-16, 64, 32, Assets.level1, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(StartState.getCopy_uiManager());
				//pentru setarea lumii
				World world=new World(handler, "res/worlds/world1.in","/worlds/Harta1.png" );

				handler.setWorld(world);

//				handler.getWorld().getEntityManager().addEntity(new Lava(handler,6* Tile.TILEWIDTH,26*Tile.TILEHEIGHT));
//				handler.getWorld().getEntityManager().addEntity(new Lava(handler,15*Tile.TILEWIDTH,21*Tile.TILEHEIGHT));
//				handler.getWorld().getEntityManager().addEntity(new Lava(handler,23*Tile.TILEWIDTH,31*Tile.TILEHEIGHT));
//				handler.getWorld().getEntityManager().addEntity(new Lava(handler,35*Tile.TILEWIDTH,31*Tile.TILEHEIGHT));
//				handler.getWorld().getEntityManager().addEntity(new Lava(handler,3*Tile.TILEWIDTH,16*Tile.TILEHEIGHT));
//				handler.getWorld().getEntityManager().addEntity(new Bandit(handler,300,200,24,32,5,0.5f));
//				handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,300,200,24,32,5,0.5f));
//				handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,250,200,24,32,5,0.5f));
				((GameState)(handler.getGame().getGameState())).setWorld(world);
				((StartState)(handler.getGame().getStartState())).setLevel(1);
				State.setState(handler.getGame().getStartState());
			}
		}));
		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32,handler.getHeight()/2+32 , 64, 32, Assets.level2, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(StartState.getCopy_uiManager());
				World world=new World(handler, "res/worlds/world2.in","/worlds/backgroundHarta2.png");
				handler.setWorld(world);

				((GameState)(handler.getGame().getGameState())).setWorld(world);
				((StartState)(handler.getGame().getStartState())).setLevel(2);
				State.setState(handler.getGame().getStartState());
			}
		}));

		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32,handler.getHeight()/2+80, 64, 32, Assets.btn_exit, new ClickListener() {

			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(null);
				System.exit(0);
			}
		}));
	}

	@Override
	public void tick() {
		uiManager.tick();

	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.menuBackground,0,0,handler.getWidth(),handler.getHeight(),null);
		uiManager.render(g);

	}


	public static UIManager getCopy_uiManager() {
		return copy_uiManager;
	}
}
