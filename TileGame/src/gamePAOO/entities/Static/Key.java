package gamePAOO.entities.Static;

import gamePAOO.Graphics.Assets;
import gamePAOO.Handler;
import gamePAOO.entities.creatures.Player;
import gamePAOO.tiles.Tile;

import java.awt.*;

public class Key extends StaticEntity {



    public Key(Handler handler, float x, float y) {

        super(handler, x, y, Tile.TILEWIDTH,Tile.TILEHEIGHT);
        bounds.x = 0;
        bounds.y = 0;
        bounds.width = width;
        bounds.height =  height;
    }

    @Override
    public void tick() {
    }

    @Override
    public void render(Graphics g) {
        if (!Player.isHasKey()) {
            g.drawImage(Assets.key, (int) (x - handler.getGameCamera().getxOffset()), (int) (y - handler.getGameCamera().getyOffset()), Tile.TILEWIDTH, Tile.TILEHEIGHT, null);
            //randare pentru teste bounding box cheie
            //g.fillRect((int) (x - handler.getGameCamera().getxOffset()), (int) (y - handler.getGameCamera().getyOffset()), bounds.width, bounds.height);
        }
    }

//    @Override
//    public boolean checkEntityCollisions(float xOffset, float yOffset) {
//        for(Entity e : handler.getWorld().getEntityManager().getEntities()){
//            if(e.equals(this))
//                continue;
//            if(e.equals(World.key)&&e.getCollisionBounds(0f, 0f).intersects(getCollisionBounds(xOffset, yOffset)))
//                return true;
//        }
//        return false;
//    }
}
