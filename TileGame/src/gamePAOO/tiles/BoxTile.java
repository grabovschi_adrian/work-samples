package gamePAOO.tiles;

import gamePAOO.Graphics.Assets;

public class BoxTile extends Tile {

	public BoxTile(int id) {
		super(Assets.box, id);
	}
	@Override
	public boolean isSolid(){
		return true;
	}


}
