package gamePAOO.entities.creatures;

import java.awt.*;
import java.awt.image.BufferedImage;

import gamePAOO.Graphics.Animation;
import gamePAOO.Handler;
import gamePAOO.Graphics.Assets;
import gamePAOO.entities.Entity;
import gamePAOO.entities.Static.Door;
import gamePAOO.entities.Static.Key;
import gamePAOO.states.LevelsState;
import gamePAOO.states.MenuState;
import gamePAOO.states.StartState;
import gamePAOO.states.State;
import gamePAOO.worlds.World;

public class Player extends Creature {

	private static boolean hasKey;
	private Animation Idle_Right,Idle_Left,Run_Right,Run_Left, fall_Right,fall_Left, jump_Right,jump_Left,attack_Right, attack_Left, die;
	private int score;
	private long time;
	private boolean firstTouchDoor;


	public Player(Handler handler, float x, float y) {
		super(handler, x, y, DEFAULT_PLAYER_WIDTH, DEFAULT_PLAYER_HEIGHT);
		bounds.x = 20;
		bounds.y = 10;
		bounds.width = 10;
		bounds.height = 24;
		hasKey=false;
		firstTouchDoor=false;
		score=0;

		Idle_Right=new Animation(animationSpeed,Assets.player_IDLE_Right);
		Idle_Left=new Animation(animationSpeed,Assets.player_IDLE_Left);
		Run_Right=new Animation(animationSpeed,Assets.playerRun_Right);
		Run_Left=new Animation(animationSpeed,Assets.playerRun_Left);
		jump_Right=new Animation(animationSpeed,Assets.jump_Right);
		jump_Left=new Animation(animationSpeed,Assets.jump_Left);
		fall_Right=new Animation(animationSpeed,Assets.fall_Right);
		fall_Left=new Animation(animationSpeed,Assets.fall_Left);
		attack_Right=new Animation(animationSpeed,Assets.attack_Right);
		attack_Left=new Animation(animationSpeed,Assets.attack_Left);
		die=new Animation(animationSpeed,Assets.player_die);
		lastDamage=System.currentTimeMillis();
		time=System.currentTimeMillis();

	}

	@Override
	public void tick() {

		AnimationsTick();

		getInput();
		move();
		handler.getGameCamera().centerOnEntity(this); //setez xOffset si yOffset al camerei
		CollisionsWithEntities();

	}

	private void getInput(){
		xMove = xVel;
		yMove = yVel;

		//caracterul mai merge putin dupa ce nu mai este apasata una din tastele pentru movement orizontal;
		// In fuctie de valoarea acestui parametru se poate spune cat de alunecoasa este podeaua.
		xVel*=0.9f;

		//Atunci cand eroul nu este pe o dala solida, acesta va cadea cu viteza "gravity".
		if(yVel<yvelMax)
			yVel+=gravity;

		if(handler.getKeyManager().jump&&canjump) {//
			yVel-=jumpspeed;
		}
		if(handler.getKeyManager().left)
			xVel = -speed;
		if(handler.getKeyManager().right) {
			xVel = speed;

		}
	}
	@Override
	public void die() {

		System.out.println("GAME OVER");

		if (System.currentTimeMillis() -a> 1000) {
			handler.getMouseManager().setUiManager(LevelsState.getCopy_uiManager());
			handler.setWorld(null);
			State.setState(handler.getGame().getLevelsState());
		}
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(getCurrentAnimationFrame(), (int) (x - handler.getGameCamera().getxOffset()), (int) (y - handler.getGameCamera().getyOffset()),width, height, null);
		for(int i=0;i<this.health;i++)
			g.drawImage(Assets.heart,i*20,handler.getHeight()-30,15,15,null);
		g.setColor(Color.BLUE);
		Font font = new Font("Caveat",Font.ITALIC,20);
		g.setFont(font);
		g.drawString("Score:"+score,0,30);
		if(health<=0)
			g.drawImage(Assets.gameOver,handler.getWidth()/2-48, handler.getHeight()/2-12,96,24, null);
		if(firstTouchDoor)
			if(((StartState)(handler.getGame().getStartState())).getLevel()==2)
				g.drawImage(Assets.wonGame,handler.getWidth()/2-100, handler.getHeight()/2-40,200,80, null);
			else
			g.drawImage(Assets.won,handler.getWidth()/2-Assets.won.getWidth()/2, handler.getHeight()/2-Assets.won.getHeight()/2,96,24, null);


		//desenez bounding box-ul

//		g.setColor(Color.red);
//		g.fillRect((int) (x + bounds.x - handler.getGameCamera().getxOffset()),
//				(int) (y + bounds.y - handler.getGameCamera().getyOffset()),
//				bounds.width, bounds.height);


		//desenez view box si attack box
//		Graphics2D g2d=(Graphics2D) g;
//		g.setColor(Color.RED);
//		g2d.draw(getRightAttackBox());
//		g2d.draw(getLeftAttackBox());
//		g.setColor(Color.BLUE);
//		g2d.draw(getRightViewBox());
//		g2d.draw(getLeftViewBox());
	}


	private BufferedImage getCurrentAnimationFrame()
	{
		if(this.health<=0)
			return die.getCurrentFrame();
		if((int)xMove>0)
			if(handler.getKeyManager().attack)
				return attack_Right.getCurrentFrame();
			else
				return Run_Right.getCurrentFrame();
		else if((int)xMove<0)
			if(handler.getKeyManager().attack)
				return attack_Left.getCurrentFrame();
			else
				return Run_Left.getCurrentFrame();
		else
			if(handler.getKeyManager().attack)
				return attack_Right.getCurrentFrame();
			else
				return Idle_Right.getCurrentFrame();

	}


	private void AnimationsTick(){
		Idle_Right.tick();
		Run_Right.tick();
		Run_Left.tick();
		jump_Left.tick();
		jump_Right.tick();
		fall_Left.tick();
		fall_Right.tick();
		attack_Right.tick();
		attack_Left.tick();
		die.tick();
	}

	public static boolean isHasKey() {
		return hasKey;
	}

	public static void setHasKey(boolean hasKey) {
		Player.hasKey = hasKey;
	}

	public boolean checkKeyCollisions(float xOffset, float yOffset){
		for(Entity e : handler.getWorld().getEntityManager().getEntities()){
			if(e.equals(this))
				continue;
			if((e instanceof Key)&&e.getCollisionBounds(0f, 0f).intersects(getCollisionBounds(xOffset, yOffset))) {
				handler.getWorld().getEntityManager().deleteEntity(e);
				score += 1000;
				return true;
			}
		}
		return false;
	}

	public boolean checkDoorCollisions(float xOffset, float yOffset){
		for(Entity e : handler.getWorld().getEntityManager().getEntities()){
			if(e.equals(this))
				continue;
			if(hasKey&&(e instanceof Door)&&(handler.getWorld().getEntityManager().getPlayer().getScore()>=1700)&&e.getCollisionBounds(0f, 0f).intersects(getCollisionBounds(xOffset, yOffset)))
			{
				if(!firstTouchDoor) {
					time = System.currentTimeMillis();
					firstTouchDoor = true;
				}
				return true;
			}
		}
		return false;
	}

	private void CollisionsWithEntities(){   // e ok
		if(checkKeyCollisions(0,0))
			hasKey=true;
		if(checkDoorCollisions(0,0)) {

			//System.out.println("Ai trecut la nivelul urmator!");
			if (System.currentTimeMillis() - time > 1000) {
				handler.getMouseManager().setUiManager(LevelsState.getCopy_uiManager());
				State.setState(handler.getGame().getLevelsState());
			}
			//handler.getMouseManager().setUiManager(MenuState.getCopy_uiManager());
			//State.setState(handler.getGame().getMenuState());
		}
		if(checkEntityCollisions(0,0)) {
			if (System.currentTimeMillis() - lastDamage > 500) {
				hurt(damage);
				lastDamage = System.currentTimeMillis();
			}
		}

	}


	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
