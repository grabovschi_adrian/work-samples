package gamePAOO.audio;


import gamePAOO.Handler;

import javax.sound.sampled.Clip;

public class MusicClip extends AudioClip {
    public MusicClip(Clip clip) {
        super(clip);
    }

    @Override
    protected float getVolume(Handler handler) {
        return handler.getGame().getGameState().getMusicVolume();
    }
}
