package gamePAOO.states;

import java.awt.Graphics;
import java.io.File;

import gamePAOO.Handler;
import gamePAOO.entities.creatures.Player;
import gamePAOO.worlds.World;



public class GameState extends State {

	private World world;



	public GameState(Handler handler){
		super(handler);
		//world = new World(handler, "res/worlds/world2.in");//creeaza harta
		//handler.setWorld(world);


	}
	
	@Override
	public void tick() {

		world.tick();
		if(handler.getKeyManager().escape) {
			handler.getMouseManager().setUiManager(StartState.getCopy_uiManager());//handler.getMouseManager().getUiManager());
			State.setState(handler.getGame().getStartState());
		}


	}

	@Override
	public void render(Graphics g) {
		world.render(g);

	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}
}
