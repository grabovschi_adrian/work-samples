package gamePAOO.tiles;

import gamePAOO.Graphics.Assets;

public class BackgroundTile extends Tile {

    public BackgroundTile(int id) {
        super(Assets.background, id);
    }

}
