package gamePAOO.states;

import gamePAOO.Graphics.Assets;
import gamePAOO.Handler;
import gamePAOO.audio.AudioPlayer;
import gamePAOO.ui.ClickListener;
import gamePAOO.ui.UIImageButton;
import gamePAOO.ui.UIManager;
import gamePAOO.ui.UISlider;

import java.awt.*;

public class OptionState extends State {

	private UIManager uiManager;
	private static UIManager copy_uiManager;

	private UISlider uiSlider;


	public OptionState(Handler handler){

		super(handler);
		uiManager=new UIManager(handler);
		handler.getMouseManager().setUiManager(uiManager);
		copy_uiManager=uiManager;
		uiSlider=new UISlider(handler.getWidth()/2-90,50,180,15,0,1,handler);//l-am facut obiect comp. pentru a putea seta volumul
		audioPlayer=new AudioPlayer(handler);
		//de setat musicVolume la 0.7
		handler.getGame().getGameState().setMusicVolume(0.7f);
		audioPlayer.playMusic("music1.wav");



		uiManager.addObject(new UIImageButton(handler.getWidth()/4-32, handler.getHeight()*4/5,64,32, Assets.btn_back, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(MenuState.getCopy_uiManager());
				State.setState(handler.getGame().getMenuState());
			}
		}));
		uiManager.addObject(new UIImageButton(handler.getWidth()*3/4-32,handler.getHeight()*4/5 , 64, 32, Assets.btn_exit, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(null);
				System.exit(0);
			}
		}));
		uiManager.addObject(uiSlider);
//		uiManager.addObject(new UIImageButton(200, 100, 128, 64, Assets.btn_start, new ClickListener() {
//				@Override
//				public void onClick() {
//					handler.getMouseManager().setUiManager(null);
//					State.setState(handler.getGame().getMenuState());
//				}
//		}));
	}

	@Override
	public void tick() {
		uiManager.tick();
		handler.getGame().getGameState().setMusicVolume((float)uiSlider.getValue());
		audioPlayer.update(handler);
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.menuBackground,0,0,handler.getWidth(),handler.getHeight(),null);
		g.drawImage(Assets.keyboard,50,100,300,100,null);
		uiManager.render(g);

	}


	public static UIManager getCopy_uiManager() {
		return copy_uiManager;
	}
}
