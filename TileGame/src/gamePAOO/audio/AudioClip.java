package gamePAOO.audio;

import gamePAOO.Handler;

import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public abstract class AudioClip {

    private final Clip clip;

    public AudioClip(Clip clip) {
        this.clip = clip;
        clip.start();
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void update(Handler handler) {
       setVolume(handler);
    }
    public void setVolume(Handler handler)
    {
        final FloatControl control = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        float range = control.getMaximum() - control.getMinimum();
        float gain = (range * getVolume(handler)) + control.getMinimum();
        control.setValue(gain);
    }

    protected abstract float getVolume(Handler handler);

    public boolean hasFinishedPlaying() {
        return !clip.isRunning();
    }

    public void cleanUp() {
        clip.close();
    }
}
