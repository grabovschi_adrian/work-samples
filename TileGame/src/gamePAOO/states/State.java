package gamePAOO.states;

import java.awt.Graphics;

import gamePAOO.Handler;
import gamePAOO.audio.AudioClip;
import gamePAOO.audio.AudioPlayer;


public abstract class State {

	protected AudioPlayer audioPlayer;
	private static State currentState = null;
	private float musicVolume=1;

	public float getMusicVolume() {
		return musicVolume;
	}

	public void setMusicVolume(float musicVolume) {
		this.musicVolume = musicVolume;
	}

	public static void setState(State state){
		currentState = state;
	}
	
	public static State getState(){
		return currentState;
	}
	
	//CLASS
	
	protected Handler handler;
	
	public State(Handler handler){
		this.handler = handler;
		audioPlayer=new AudioPlayer(handler);
	}
	
	public abstract void tick( );
	
	public abstract void render(Graphics g);
	
}
