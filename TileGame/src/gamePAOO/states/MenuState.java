package gamePAOO.states;

import java.awt.*;


import gamePAOO.Graphics.Assets;
import gamePAOO.Handler;
import gamePAOO.ui.ClickListener;
import gamePAOO.ui.UIImageButton;
import gamePAOO.ui.UIManager;

public class MenuState extends State {

	private UIManager uiManager;
	private static UIManager copy_uiManager;


	public MenuState(Handler handler){

		super(handler);
		uiManager=new UIManager(handler);
		handler.getMouseManager().setUiManager(uiManager);
		copy_uiManager=uiManager;


		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32, handler.getHeight()/2-16, 64, 32, Assets.btn_start, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(LevelsState.getCopy_uiManager());
				State.setState(handler.getGame().getLevelsState());
			}
		}));
		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32,handler.getHeight()/2+32 , 64, 32, Assets.btn_options, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(OptionState.getCopy_uiManager());
				State.setState(handler.getGame().getOptionState());
			}
		}));

		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32,  handler.getHeight()/2+80, 64, 32, Assets.btn_exit, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(null);
				System.exit(0);
			}
		}));

	}

	@Override
	public void tick() {
		uiManager.tick();
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.menuBackground,0,0,handler.getWidth(),handler.getHeight(),null);
		g.setColor(new Color(14, 220, 239, 255));
		Font font = new Font("Caveat",Font.ITALIC,25);
		Font font1 = new Font("Caveat",Font.ITALIC,16);
		g.setFont(font);
		//g.fillRect(handler.getWidth()/2-110,30,220,50);
		//g.setColor(Color.);
		g.setColor(new Color(14, 168, 239, 255));
		g.drawString("M16 Adventures!",handler.getWidth()/2-80,60);
		g.drawString("The lost teammate",handler.getWidth()/2-100,80);
		uiManager.render(g);
	}


	public static UIManager getCopy_uiManager() {
		return copy_uiManager;
	}
}
