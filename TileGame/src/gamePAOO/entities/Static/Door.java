package gamePAOO.entities.Static;

import gamePAOO.Graphics.Assets;
import gamePAOO.Handler;
import gamePAOO.entities.creatures.Player;
import gamePAOO.tiles.Tile;

import java.awt.*;

public class Door extends StaticEntity {



    public Door(Handler handler, float x, float y) {
        super(handler, x, y, 3*Tile.TILEWIDTH,4*Tile.TILEHEIGHT);
    }

    @Override
    public void tick() {
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.door,(int)(x - handler.getGameCamera().getxOffset()),(int)(y - handler.getGameCamera().getyOffset()),16*3,16*4,null);
    }


}
