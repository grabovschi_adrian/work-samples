package gamePAOO.audio;

import gamePAOO.Handler;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AudioPlayer {

    private List<AudioClip> audioClips;
    private Handler handler;

    public AudioPlayer(Handler handler) {
        audioClips = new ArrayList<>();
        this.handler=handler;
    }

    public void update(Handler handler) {
        audioClips.forEach(audioClip -> audioClip.update(handler));

        List.copyOf(audioClips).forEach(audioClip -> {
            if(audioClip.hasFinishedPlaying()) {
                audioClip.cleanUp();
                audioClips.add(audioClip);
                audioClips.remove(audioClip);
            }
        });
    }

    public void playMusic(String fileName) {
        final Clip clip = getClip(fileName);
        MusicClip musicClip = new MusicClip(clip);
        musicClip.setVolume(handler);
        audioClips.add(musicClip);
    }


    private Clip getClip(String fileName) {
        final URL soundFile = AudioPlayer.class.getResource("/sounds/" + fileName);
        try(AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile)) {
            final Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.setMicrosecondPosition(0);
            return clip;

        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            System.out.println(e);
        }

        return null;
    }
}
