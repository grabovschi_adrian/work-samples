package gamePAOO.entities.creatures;

import gamePAOO.Graphics.Animation;
import gamePAOO.Graphics.Assets;
import gamePAOO.Handler;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Bandit extends Enemy{

    private Animation bandit_Idle_Left,bandit_Idle_Right,bandit_Attack_Left,bandit_Attack_Right,bandit_Hit_Left, bandit_Hit_Right,bandit_Dead, bandit_Run_Right, bandit_Run_Left;


    public Bandit(Handler handler, float x, float y, int width, int height, int health, float damage) {
        super(handler, x, y, width, height, health, damage);
        bounds.x = -3;
        bounds.y = 0;
        bounds.width = 30;
        bounds.height = 34;

        bandit_Idle_Left=new Animation(animationSpeed, Assets.bandit_Idle_Left);
        bandit_Idle_Right=new Animation(animationSpeed, Assets.bandit_Idle_Right);
        bandit_Attack_Left=new Animation(animationSpeed, Assets.bandit_Attack_Left);
        bandit_Attack_Right=new Animation(animationSpeed, Assets.bandit_Attack_Right);
        bandit_Hit_Left=new Animation(animationSpeed, Assets.bandit_Hit_Left);
        bandit_Hit_Right=new Animation(animationSpeed, Assets.bandit_Hit_Right);
       // bandit_Dead=new Animation(animationSpeed, Assets.bandit_Dead);
        bandit_Run_Right=new Animation(animationSpeed, Assets.bandit_Run_Right);
        bandit_Run_Left=new Animation(animationSpeed, Assets.bandit_Run_Left);
        lastDamage=System.currentTimeMillis();
    }
    @Override
    public void tick() {

        AnimationsTick();

        getInput();
        move();

        CollisionsWithEntities();

    }
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(), (int) (x - handler.getGameCamera().getxOffset()), (int) (y - handler.getGameCamera().getyOffset()), width, height, null);

        //desenez bounding box-ul

//		g.setColor(Color.red);
//		g.fillRect((int) (x + bounds.x - handler.getGameCamera().getxOffset()),
//				(int) (y + bounds.y - handler.getGameCamera().getyOffset()),
//				bounds.width, bounds.height);

        //desenez view box si attack box
//        Graphics2D g2d=(Graphics2D) g;
//        g.setColor(Color.RED);
//        g2d.draw(getRightAttackBox());
//        g2d.draw(getLeftAttackBox());
//        g.setColor(Color.BLUE);
//        g2d.draw(getRightViewBox());
//        g2d.draw(getLeftViewBox());

    }


    private BufferedImage getCurrentAnimationFrame()
    {

        if(this.health<=0)
            return bandit_Dead.getCurrentFrame();
        if((int)xMove>0)
            if(this.getRightAttackBox().intersects(handler.getWorld().getEntityManager().getPlayer().getLeftAttackBox()))
                return bandit_Attack_Right.getCurrentFrame();
            else
                return bandit_Run_Right.getCurrentFrame();
        else if((int)xMove<0)
            if(this.getLeftAttackBox().intersects(handler.getWorld().getEntityManager().getPlayer().getRightAttackBox()))
                return bandit_Attack_Left.getCurrentFrame();
            else
                return bandit_Run_Left.getCurrentFrame();
        else
        if(this.getRightAttackBox().intersects(handler.getWorld().getEntityManager().getPlayer().getLeftAttackBox()))
            return bandit_Attack_Right.getCurrentFrame();
        else
            return bandit_Idle_Right.getCurrentFrame();



    }

    private void getInput(){
        xMove = xVel;
        yMove = yVel;

        //caracterul mai merge putin dupa ce nu mai este apasata una din tastele pentru movement orizontal;
        // In fuctie de valoarea acestui parametru se poate spune cat de alunecoasa este podeaua.
        xVel*=0.9f;

        //Atunci cand eroul nu este pe o dala solida, acesta va cadea cu viteza "gravity".
        yVel+=gravity;

        if(this.getLeftViewBox().intersects(handler.getWorld().getEntityManager().getPlayer().getRightViewBox()))
            xVel = -speed;
        if(this.getRightViewBox().intersects(handler.getWorld().getEntityManager().getPlayer().getLeftViewBox())){
            xVel = speed;
        }
//        if(this.getCollisionBounds(0,0).intersects(handler.getWorld().getEntityManager().getPlayer().getLeftAttackBox())||
//                this.getCollisionBounds(0,0).intersects(handler.getWorld().getEntityManager().getPlayer().getRightViewBox()))
        if(handler.getKeyManager().attack&&((this.getRightAttackBox().intersects(handler.getWorld().getEntityManager().getPlayer().getLeftAttackBox())&&handler.getKeyManager().left) ||
                this.getLeftAttackBox().intersects(handler.getWorld().getEntityManager().getPlayer().getRightAttackBox()))) {
           // hurt(handler.getWorld().getEntityManager().getPlayer().getDamage());
            if (System.currentTimeMillis() - lastDamage > 100) {
                hurt(handler.getWorld().getEntityManager().getPlayer().getDamage());
                lastDamage = System.currentTimeMillis();
            }
        }
    }

    private void AnimationsTick(){
        bandit_Hit_Right.tick();
        bandit_Idle_Right.tick();
        bandit_Run_Right.tick();
        bandit_Run_Left.tick();
        bandit_Attack_Right.tick();
        bandit_Attack_Left.tick();
        //bandit_Dead.tick();
        bandit_Hit_Left.tick();
    }

    @Override
    public void die() {
        handler.getWorld().getEntityManager().getPlayer().setScore(handler.getWorld().getEntityManager().getPlayer().getScore()+100);
        //if(System.currentTimeMillis()-anim>900)
            handler.getWorld().getEntityManager().deleteEntity(this);
    }
}
