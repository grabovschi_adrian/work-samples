package gamePAOO.entities.creatures;

import gamePAOO.Handler;
import gamePAOO.entities.Entity;
import gamePAOO.tiles.Tile;

import java.awt.*;

public abstract class Creature extends Entity {

	public float last_xMove=0;
	protected long a;
	protected long lastDamage;
	public static final int DEFAULT_HEALTH = 10;
	public static final float DEFAULT_SPEED = 3.0f;
	public static final int DEFAULT_PLAYER_WIDTH = 50,
							DEFAULT_PLAYER_HEIGHT = 37;

	protected float health;
	protected float speed;
	protected float gravity=1f;
	protected float jumpspeed=8;
	protected float xMove, yMove, xVel, yVel, yvelMax=10;
	protected boolean canjump=true;
	protected int animationSpeed =100;
	protected float damage;

	public Creature(Handler handler, float x, float y, int width, int height) {
		super(handler, x, y, width, height);
		health = DEFAULT_HEALTH;
		speed = DEFAULT_SPEED;
		xMove = 0;
		yMove = 0;
		yVel=0;
		xVel=0;
		damage=1;
	}
	
	public void move(){
			moveX();
			moveY();
	}
	
	public void moveX(){
		if(xMove > 0){//Moving right
			int tx = (int) (x + xMove + bounds.x + bounds.width) / Tile.TILEWIDTH;

			if(!collisionWithTile(tx, (int) (y + bounds.y) / Tile.TILEHEIGHT) &&
					!collisionWithTile(tx, (int) (y + bounds.y + bounds.height) / Tile.TILEHEIGHT)){
				x += xMove;
			}
		}else if(xMove < 0){//Moving left
			int tx = (int) (x + xMove + bounds.x) / Tile.TILEWIDTH;

			if(!collisionWithTile(tx, (int) (y + bounds.y) / Tile.TILEHEIGHT) &&
					!collisionWithTile(tx, (int) (y + bounds.y + bounds.height) / Tile.TILEHEIGHT)){
				x += xMove;
			}
		}
		last_xMove=xMove;
	}
	
	public void moveY(){

		if(yMove < 0){//Up
			int ty = (int) (y + yMove + bounds.y) / Tile.TILEHEIGHT;

			if(!collisionWithTile((int) (x + bounds.x) / Tile.TILEWIDTH, ty) &&
					!collisionWithTile((int) (x + bounds.x + bounds.width) / Tile.TILEWIDTH, ty)){
				y += yMove;
				canjump=false;
			}
		}else if(yMove > 0){//Down
			int ty = (int) (y + yMove + bounds.y + bounds.height) / Tile.TILEHEIGHT;

			if(!collisionWithTile((int) (x + bounds.x) / Tile.TILEWIDTH, ty) &&
					!collisionWithTile((int) (x + bounds.x + bounds.width) / Tile.TILEWIDTH, ty)){
				y += yMove;
			}
			else
			{
				y=ty*Tile.TILEHEIGHT-bounds.y-bounds.height-1;
				yVel=0;
				canjump=true;
			}

		}
	}
	public abstract void die();

	public void hurt(float damage) {

		this.health -= damage;
		if(this.health==0)
			a=System.currentTimeMillis();
		if (this.health <= 0) {
			die();
		}
	}

	public Rectangle getRightViewBox() {
		return new Rectangle((int) (x + (width)-handler.getGameCamera().getxOffset()), (int) (y-handler.getGameCamera().getyOffset()),
				(int) ((width * 2)), (int) (height));
	}
	public Rectangle getLeftViewBox() {
		return new Rectangle((int) (x - (width * 2)-handler.getGameCamera().getxOffset()), (int) (y-handler.getGameCamera().getyOffset()),
				(int) ((width * 2)), (int) (height));
	}

	public Rectangle getRightAttackBox() {
		return new Rectangle((int) (x -handler.getGameCamera().getxOffset()), (int) (y-handler.getGameCamera().getyOffset()),
				(int) ((width)), (int) (height));
	}
	public Rectangle getLeftAttackBox() {
		return new Rectangle((int) (x -handler.getGameCamera().getxOffset()), (int) (y-handler.getGameCamera().getyOffset()),
				(int) ((width)), (int) (height));
	}

	protected boolean collisionWithTile(int x, int y){
		return handler.getWorld().getTile(x, y).isSolid();
	}
	
	//GETTERS SETTERS

	public float getxMove() {
		return xMove;
	}

	public void setxMove(float xMove) {
		this.xMove = xMove;
	}

	public float getyMove() {
		return yMove;
	}

	public void setyMove(float yMove) {
		this.yMove = yMove;
	}

	public float getHealth() {
		return health;
	}

	public float getDamage() {
		return damage;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
}
