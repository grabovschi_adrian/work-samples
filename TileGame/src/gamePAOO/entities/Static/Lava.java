package gamePAOO.entities.Static;

import gamePAOO.Graphics.Assets;
import gamePAOO.Handler;
import gamePAOO.entities.creatures.Player;
import gamePAOO.tiles.Tile;

import java.awt.*;

public class Lava extends StaticEntity {


    public Lava(Handler handler, float x, float y) {
        super(handler, x, y, 4*Tile.TILEWIDTH,Tile.TILEHEIGHT);
        bounds.x = 0;
        bounds.y = 0;
        bounds.width = width;
        bounds.height =  height;

    }

    @Override
    public void tick() {
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.lava,(int)(x - handler.getGameCamera().getxOffset()),(int)(y - handler.getGameCamera().getyOffset()),16*4,16,null);
//        g.setColor(Color.red);
//		g.fillRect((int) (x + bounds.x - handler.getGameCamera().getxOffset()),
//				(int) (y + bounds.y - handler.getGameCamera().getyOffset()),
//				bounds.width, bounds.height);
    }


}
