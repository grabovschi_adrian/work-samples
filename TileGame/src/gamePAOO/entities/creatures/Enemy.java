package gamePAOO.entities.creatures;

import gamePAOO.Handler;


public abstract  class Enemy extends Creature {
    private float enemySpeed=1.5f;

    public Enemy(Handler handler, float x, float y, int width, int height, int health, float damage) {
        super(handler, x, y, width, height);
        this.health=health;
        this.speed=enemySpeed;
        this.damage=damage;
    }

    protected void CollisionsWithEntities(){   // e ok

        if(checkEntityCollisions(0,0)) {
            if (System.currentTimeMillis() - lastDamage > 500) {
                hurt(handler.getWorld().getEntityManager().getPlayer().getDamage());
                lastDamage = System.currentTimeMillis();
            }
        }

    }




}
