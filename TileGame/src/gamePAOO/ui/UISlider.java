package gamePAOO.ui;

import gamePAOO.Handler;
import gamePAOO.entities.Entity;
import gamePAOO.entities.Static.Lava;
import gamePAOO.entities.creatures.Bandit;
import gamePAOO.entities.creatures.Skeleton;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.sql.*;

public class UISlider extends UIObject {

	private double value;
	private double min;
	private double max;
	private Handler handler;

	public UISlider(float x, float y, int width, int height,  double min, double max, Handler handler) {
		super(x, y, width, height);
		//this.images = images;
		//this.clicker = clicker;
		this.min = min;
		this.max = max;
		this.value = max;
		this.handler=handler;
	}

	@Override
	public void tick() {

	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect((int)x-10, (int)y-15, width+20, height+20);
		Font font = new Font("Serif", Font.BOLD, 25);

		g.setColor(Color.RED);
		g.fillRect((int)x, (int)y, width, height);

		g.setColor(Color.BLACK);
		g.fillRect((int)x, (int)y, getPixelsOfCurrentValue(), height);
		g.drawString("Volume: "+(int)(handler.getGame().getGameState().getMusicVolume()*100),(int)x, (int)y-5);
	}

	@Override
	public void onClick() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:database.db");
			c.setAutoCommit(true);
			stmt = c.createStatement();

			String sql = "CREATE TABLE IF NOT EXISTS  Volume" +
					" (Volume REAL NOT NULL)";
			stmt.executeUpdate(sql);

			stmt.executeUpdate("DELETE FROM Volume");

			sql = "INSERT INTO Volume(Volume) VALUES (?);";

			double val = getValueAt(handler.getMouseManager().getMouseX());
			PreparedStatement preparedStatement = c.prepareStatement(sql);
			preparedStatement.setDouble(1, val);
			preparedStatement.executeUpdate();


			ResultSet rs = stmt.executeQuery("SELECT * FROM Volume");
			this.value=rs.getDouble("Volume");

			stmt.close();
			c.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}


		//this.value = getValueAt(handler.getMouseManager().getMouseX());
	}

	private double getValueAt(double xPosition) {
		double positionOnSlider = xPosition - x;//absolutePosition.getX();
		double percentage = positionOnSlider / width;
		double range = max - min;

		return min + range * percentage;
	}

	private int getPixelsOfCurrentValue() {
		double range = max - min;
		double percentage = value - min;

		return (int) ((percentage / range) * width);
	}

	public double getValue() {
		return value;
	}
}
