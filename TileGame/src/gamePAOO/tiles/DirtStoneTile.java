package gamePAOO.tiles;

import gamePAOO.Graphics.Assets;

public class DirtStoneTile extends Tile {

	public DirtStoneTile(int id) {
		super(Assets.dirtStone, id);
	}
	@Override
	public boolean isSolid(){
		return true;
	}


}
