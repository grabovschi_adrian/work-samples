package gamePAOO.states;

import gamePAOO.Graphics.Assets;
import gamePAOO.Handler;
import gamePAOO.entities.Static.Door;
import gamePAOO.entities.Static.Key;
import gamePAOO.entities.Static.Lava;
import gamePAOO.entities.creatures.Bandit;
import gamePAOO.entities.creatures.Player;
import gamePAOO.entities.creatures.Skeleton;
import gamePAOO.tiles.Tile;
import gamePAOO.ui.ClickListener;
import gamePAOO.ui.UIImageButton;
import gamePAOO.ui.UIManager;

import java.awt.*;
import java.io.File;

public class StartState extends State {

	private UIManager uiManager;
	private static UIManager copy_uiManager;
	private int level;


	public StartState(Handler handler){

		super(handler);
		uiManager=new UIManager(handler);
		handler.getMouseManager().setUiManager(uiManager);
		copy_uiManager=uiManager;


		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32, handler.getHeight()/2-112,64,32, Assets.btn_back, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(LevelsState.getCopy_uiManager());
				State.setState(handler.getGame().getLevelsState());

			}
		}));uiManager.addObject(new UIImageButton(handler.getWidth()/2-32, handler.getHeight()/2-64,64,32, Assets.btn_continue, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(null);
				State.setState(handler.getGame().getGameState());

			}
		}));
		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32,handler.getHeight()/2-16, 64, 32, Assets.btn_new, new ClickListener() {
			@Override
			public void onClick() {
				handler.getMouseManager().setUiManager(null);
				//pentru setarea lumii
//				World world=new World(handler, "res/worlds/world1.in","/worlds/Harta1.png" );
				handler.getWorld().getEntityManager().setPlayer( new Player(handler,handler.getWorld().getSpawnX(),handler.getWorld().getSpawnY()));
//				handler.setWorld(world);
				handler.getWorld().getEntityManager().addEntity(new Key(handler,handler.getWorld().getKeySpawnX(),handler.getWorld().getKeySpawnY()));
				handler.getWorld().getEntityManager().addEntity(new Door(handler,handler.getWorld().getDoorSpawnX(),handler.getWorld().getDoorSpawnY()));
				if(level ==1) {
					handler.getWorld().getEntityManager().addEntity(new Lava(handler, 7 * Tile.TILEWIDTH, 16 * Tile.TILEHEIGHT));
					handler.getWorld().getEntityManager().addEntity(new Lava(handler, 18 * Tile.TILEWIDTH, 16 * Tile.TILEHEIGHT));
					handler.getWorld().getEntityManager().addEntity(new Lava(handler, 6 * Tile.TILEWIDTH, 7 * Tile.TILEHEIGHT));
					handler.getWorld().getEntityManager().addEntity(new Lava(handler, 13 * Tile.TILEWIDTH, 9 * Tile.TILEHEIGHT));

					handler.getWorld().getEntityManager().addEntity(new Bandit(handler, 13 * Tile.TILEWIDTH, 15 * Tile.TILEHEIGHT, 30,40, 7, 0.5f));
					handler.getWorld().getEntityManager().addEntity(new Bandit(handler, 23 * Tile.TILEWIDTH, 10 * Tile.TILEHEIGHT, 30,40, 7, 0.5f));
					handler.getWorld().getEntityManager().addEntity(new Bandit(handler, 6 * Tile.TILEWIDTH, 6 * Tile.TILEHEIGHT, 30,40, 7, 0.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,23 * Tile.TILEWIDTH, 8 * Tile.TILEHEIGHT,30,40,5,0.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler, 20 * Tile.TILEWIDTH, 11 * Tile.TILEHEIGHT, 30, 40, 5, 0.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler, 15 * Tile.TILEWIDTH, 14 * Tile.TILEHEIGHT, 30, 40, 5, 0.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler, 23 * Tile.TILEWIDTH, 5 * Tile.TILEHEIGHT, 30, 40, 5, 0.5f));

				}
				else if(level ==2){
					handler.getWorld().getEntityManager().getPlayer().setHealth(7);
					handler.getWorld().getEntityManager().addEntity(new Lava(handler,6* Tile.TILEWIDTH,26*Tile.TILEHEIGHT));
					handler.getWorld().getEntityManager().addEntity(new Lava(handler,15*Tile.TILEWIDTH,21*Tile.TILEHEIGHT));
					handler.getWorld().getEntityManager().addEntity(new Lava(handler,23*Tile.TILEWIDTH,31*Tile.TILEHEIGHT));
					handler.getWorld().getEntityManager().addEntity(new Lava(handler,35*Tile.TILEWIDTH,31*Tile.TILEHEIGHT));
					handler.getWorld().getEntityManager().addEntity(new Lava(handler,13*Tile.TILEWIDTH,15*Tile.TILEHEIGHT));

					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,17*Tile.TILEWIDTH,30*Tile.TILEHEIGHT,30, 40,5,1.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,7*Tile.TILEWIDTH,17*Tile.TILEHEIGHT,30, 40,5,1.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,13*Tile.TILEWIDTH,15*Tile.TILEHEIGHT,30, 40,5,1.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,13*Tile.TILEWIDTH,15*Tile.TILEHEIGHT,30, 40,5,1.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,41*Tile.TILEWIDTH,30*Tile.TILEHEIGHT,30, 40,5,1.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,13*Tile.TILEWIDTH,15*Tile.TILEHEIGHT,30, 40,5,1.5f));
					handler.getWorld().getEntityManager().addEntity(new Skeleton(handler,37*Tile.TILEWIDTH,7*Tile.TILEHEIGHT,30, 40,5,1.5f));
					handler.getWorld().getEntityManager().addEntity(new Bandit(handler, 12*Tile.TILEWIDTH,15*Tile.TILEHEIGHT, 30, 40, 5, 1.5f));
					handler.getWorld().getEntityManager().addEntity(new Bandit(handler, 40*Tile.TILEWIDTH,16*Tile.TILEHEIGHT, 30, 40, 5, 1.5f));
					handler.getWorld().getEntityManager().addEntity(new Bandit(handler, 34*Tile.TILEWIDTH,7*Tile.TILEHEIGHT, 30, 40, 5, 1.5f));
					handler.getWorld().getEntityManager().addEntity(new Bandit(handler, 41*Tile.TILEWIDTH,22*Tile.TILEHEIGHT, 30, 40, 5, 1.5f));
					handler.getWorld().getEntityManager().addEntity(new Bandit(handler, 25*Tile.TILEWIDTH,20*Tile.TILEHEIGHT, 30, 40, 5, 1.5f));

				}

				State.setState(handler.getGame().getGameState());
			}
		}));
		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32,handler.getHeight()/2+32 , 64, 32, Assets.btn_save, new ClickListener() {
			@Override
			public void onClick() {
				String path = null, tableName=null;
				if(level==1) {
					path = "save1";
					tableName = "Level1";
				}
				else if(level==2)
				{
					path="save2";
					tableName="Level2";
				}
				try{
					File saveFile=new File(path);
					if(saveFile.createNewFile())
						System.out.println("File created "+ saveFile.getName());
					else
						System.out.println("File already exists.");
					handler.getWorld().saveGame(saveFile, tableName);
				} catch (Exception e) {
					e.printStackTrace();
				}


			}
		}));

		uiManager.addObject(new UIImageButton(handler.getWidth()/2-32,handler.getHeight()/2+80, 64, 32, Assets.btn_load, new ClickListener() {

			@Override
			public void onClick() {
				String path = null,tableName=null;
				if(level==1) {
					path = "save1";
					tableName = "Level1";
				}
				else if(level==2) {
					path = "save2";
					tableName = "Level2";
				}
				handler.getWorld().loadGame(path,tableName);
				handler.getMouseManager().setUiManager(null);
				State.setState(handler.getGame().getGameState());

			}
		}));
	}

	@Override
	public void tick() {
		uiManager.tick();

	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.menuBackground,0,0,handler.getWidth(),handler.getHeight(),null);
		uiManager.render(g);
	}


	public static UIManager getCopy_uiManager() {
		return copy_uiManager;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
