package gamePAOO.worlds;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.sql.*;

import gamePAOO.Graphics.Assets;
import gamePAOO.Graphics.ImageLoader;
import gamePAOO.Handler;
import gamePAOO.entities.Entity;
import gamePAOO.entities.EntityManager;
import gamePAOO.entities.Static.Door;
import gamePAOO.entities.Static.Key;
import gamePAOO.entities.Static.Lava;
import gamePAOO.entities.creatures.Bandit;
import gamePAOO.entities.creatures.Player;
import gamePAOO.entities.creatures.Skeleton;
import gamePAOO.states.GameState;
import gamePAOO.tiles.Tile;
import gamePAOO.utils.Utils;

public class World {

	private Handler handler;
	private int width, height;
	private int spawnX, spawnY;
	private int keySpawnX, keySpawnY;
	private int doorSpawnX,doorSpawnY;
	private int[][] tiles;
	private BufferedImage background;
	private EntityManager entityManager;

	public World(Handler handler, String pathWorld, String pathBackground){
		this.handler = handler;
		loadWorld(pathWorld);
		entityManager=new EntityManager(handler,new Player(handler,spawnX,spawnY));
		background= ImageLoader.loadImage(pathBackground);


		//entityManager.addEntity(new Key(handler,keySpawnX,keySpawnY));
		//entityManager.addEntity(new Door(handler,doorSpawnX,doorSpawnY));
//		entityManager.addEntity(new Lava(handler,6*Tile.TILEWIDTH,26*Tile.TILEHEIGHT));
//		entityManager.addEntity(new Lava(handler,15*Tile.TILEWIDTH,21*Tile.TILEHEIGHT));
//		entityManager.addEntity(new Lava(handler,23*Tile.TILEWIDTH,31*Tile.TILEHEIGHT));
//		entityManager.addEntity(new Lava(handler,35*Tile.TILEWIDTH,31*Tile.TILEHEIGHT));
//		entityManager.addEntity(new Lava(handler,3*Tile.TILEWIDTH,16*Tile.TILEHEIGHT));
//
//		entityManager.addEntity(new Skeleton(handler,300,200,24,32,5,0.5f));
//		entityManager.addEntity(new Skeleton(handler,250,200,24,32,5,0.5f));
	}
	
	public void tick(){
		entityManager.tick();
	}
	
	public void render(Graphics g){
		int xStart = (int) Math.max(0, handler.getGameCamera().getxOffset() / Tile.TILEWIDTH);
		int xEnd = (int) Math.min(width, (handler.getGameCamera().getxOffset() + handler.getWidth()) / Tile.TILEWIDTH +1 );
		int yStart = (int) Math.max(0, handler.getGameCamera().getyOffset() / Tile.TILEHEIGHT);
		int yEnd = (int) Math.min(height, (handler.getGameCamera().getyOffset() + handler.getHeight()) / Tile.TILEHEIGHT+1);

		int xStart_img = (int) Math.max(0, handler.getGameCamera().getxOffset() );
		int xEnd_img = (int) Math.min(width*Tile.TILEHEIGHT, (handler.getGameCamera().getxOffset() + handler.getWidth())  +1 );
		int yStart_img = (int) Math.max(0, handler.getGameCamera().getyOffset() );
		int yEnd_img = (int) Math.min(height*Tile.TILEHEIGHT, (handler.getGameCamera().getyOffset() + handler.getHeight()) +1);


		g.drawImage(background.getSubimage(xStart_img,yStart_img,(xEnd_img-xStart_img),(yEnd_img-yStart_img)),0,0,null);
		for(int y = yStart;y < yEnd;y++){
			for(int x = xStart;x < xEnd;x++){
				getTile(x, y).render(g, (int) (x * Tile.TILEWIDTH - handler.getGameCamera().getxOffset()),
						(int) (y * Tile.TILEHEIGHT - handler.getGameCamera().getyOffset()));
			}
		}

		entityManager.render(g);

	}
	
	public Tile getTile(int x, int y){
		if(x < 0 || y < 0 || x >= width || y >= height)
			return Tile.backgroundTile;
		
		Tile t = Tile.tiles[tiles[x][y]];
		if(t == null)
			return Tile.backgroundTile;
		return t;
	}
	
	private void loadWorld(String path){
		String file = Utils.loadFileAsString(path);
		String[] tokens = file.split("\\s+");
		width = Utils.parseInt(tokens[0]);
		height = Utils.parseInt(tokens[1]);
		spawnX = Utils.parseInt(tokens[2]);
		spawnY = Utils.parseInt(tokens[3]);
		keySpawnX = Utils.parseInt(tokens[4]);
		keySpawnY = Utils.parseInt(tokens[5]);
		doorSpawnX = Utils.parseInt(tokens[6]);
		doorSpawnY = Utils.parseInt(tokens[7]);
		tiles = new int[width][height];
		for(int y = 0;y < height;y++){
			for(int x = 0;x < width;x++){
				tiles[x][y] = Utils.parseInt(tokens[(x + y * width) + 8]);
			}
		}
	}

	public void saveGame(File out, String tableName){
		try{
			FileWriter myWriter=new FileWriter(out.getName());
			myWriter.write((int)entityManager.getPlayer().getX()+" "+(int)entityManager.getPlayer().getY()+"\n");
			myWriter.write((int)entityManager.getPlayer().getHealth()+"\n"); // cate vieti are playerul
			myWriter.write(entityManager.getPlayer().getScore()+"\n");
			if(Player.isHasKey())
				myWriter.write("1\n");
			else
				myWriter.write("0\n");
			myWriter.write((int)entityManager.getEntities().get(0).getX()+" "+(int)entityManager.getEntities().get(0).getY()+"\n"); //coordonate cheie
			myWriter.write((int)entityManager.getEntities().get(1).getX()+" "+(int)entityManager.getEntities().get(1).getY()+"\n"); // coordonate usa


			myWriter.write(entityManager.getEntities().size()-2+"\n");//numarul de inamici


			Connection c = null;
			Statement stmt = null;
			try {
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:database.db");
				c.setAutoCommit(true);
				stmt = c.createStatement();

				String sql = "CREATE TABLE IF NOT EXISTS " +tableName+
						" (Type TEXT NOT NULL, " +
						" ChrX INT NOT NULL, " +
						" ChrY INT NOT NULL)";
				stmt.executeUpdate(sql);


				stmt.executeUpdate("DELETE FROM "+tableName);

				for(int i = 2;i < entityManager.getEntities().size();i++){
					Entity e = entityManager.getEntities().get(i);
					String ch = null;
					int x,y;
					if(e instanceof Lava)
						ch="l";
					else if(e instanceof  Skeleton)
						ch="s";
					else if(e instanceof Bandit)
						ch="b";
					x=(int)e.getX();
					y=(int)e.getY();

					sql="INSERT INTO "+tableName+ "(Type, ChrX, ChrY) VALUES (?,?,?);";

					PreparedStatement preparedStatement=c.prepareStatement(sql);
					preparedStatement.setString(1,ch);
					preparedStatement.setInt(2,x);
					preparedStatement.setInt(3,y);

					preparedStatement.executeUpdate();

					entityManager.getEntities().remove(e);

				}

				System.out.println("\n\n\n");

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException throwables) {
				throwables.printStackTrace();
			}

			stmt.close();
			c.close();


			myWriter.close();
			System.out.println("Print successfully");
		} catch (Exception e) {
			System.out.println("An error occurred while writing to the file!");
			e.printStackTrace();
		}
	}

	public void loadGame(String path, String tableName){
		int enemyCount;
		int key;
		int contor=0;
		String ch;

		String file=Utils.loadFileAsString(path);
		String[] tokens=file.split("\\s+");

		entityManager.getPlayer().setX(Utils.parseInt(tokens[contor++]));
		entityManager.getPlayer().setY(Utils.parseInt(tokens[contor++]));
		entityManager.getPlayer().setHealth(Utils.parseInt(tokens[contor++]));
		entityManager.getPlayer().setScore(Utils.parseInt(tokens[contor++]));
		key=Utils.parseInt(tokens[contor++]);
		if(key==0) {
			Player.setHasKey(false);
			entityManager.addEntity( new Key(handler,Utils.parseInt(tokens[contor++]),Utils.parseInt(tokens[contor++])));
		}
		else
			Player.setHasKey(true);



		entityManager.addEntity( new Door(handler,Utils.parseInt(tokens[contor++]),Utils.parseInt(tokens[contor++])));

		enemyCount=Utils.parseInt(tokens[contor++]);
		if(enemyCount>0) {
//			for(int i=0;i<enemyCount;i++){
//				c=tokens[contor++];
//
//				if(c.charAt(0)=='l')
//					entityManager.addEntity(new Lava(handler,Utils.parseInt(tokens[contor++]),Utils.parseInt(tokens[contor++])));
//				else if(c.charAt(0)=='s')
//					entityManager.addEntity(new Skeleton(handler,Utils.parseInt(tokens[contor++]),Utils.parseInt(tokens[contor++]),24,32,5,0.5f));
//				else if(c.charAt(0)=='b')
//					entityManager.addEntity(new Bandit(handler,Utils.parseInt(tokens[contor++]),Utils.parseInt(tokens[contor++]),24,32,5,0.5f));
//			}
			Connection c = null;
			Statement stmt = null;
			try {
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:database.db");
				c.setAutoCommit(true);
				stmt = c.createStatement();

				ResultSet rs = stmt.executeQuery("SELECT * FROM "+tableName);
				while (rs.next()) {
					String type = rs.getString("Type");
					int chrX = rs.getInt("ChrX");
					int chrY = rs.getInt("ChrY");
					if(type.charAt(0)=='l')
						entityManager.addEntity(new Lava(handler,chrX,chrY));
					else if(type.charAt(0)=='s')
						entityManager.addEntity(new Skeleton(handler,chrX,chrY,24,32,5,0.5f));
					else if(type.charAt(0)=='b')
						entityManager.addEntity(new Bandit(handler,chrX,chrY,24,32,5,0.5f));



				}
				stmt.close();
				c.close();

			} catch (ClassNotFoundException e) {
				System.out.println("Eroare la incarcararea din baza de date!\n\n");
				e.printStackTrace();
				System.exit(1);
			} catch (SQLException throwables) {
				System.out.println("Eroare la incarcararea din baza de date!\n\n");
				throwables.printStackTrace();
				System.exit(1);
			}

		}


	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getSpawnX() {
		return spawnX;
	}

	public int getSpawnY() {
		return spawnY;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public EntityManager getEntityManager() { return entityManager; }

	public int getKeySpawnX() {
		return keySpawnX;
	}

	public int getKeySpawnY() {
		return keySpawnY;
	}

	public int getDoorSpawnX() {
		return doorSpawnX;
	}

	public int getDoorSpawnY() {
		return doorSpawnY;
	}

}








